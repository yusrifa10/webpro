<?php

    class database_flickr extends CI_Model
{
    public function daftar_akun($data)
    {
        $param = array(
            "email"=>$data['email'],
            "password"=>$data['password']
        );
        $insert = $this->db->insert('akun', $param);
        if ($insert){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function login_akun($data){
        $this->db->where('email',$data['email']);
        $this->db->where('password',$data['password']);

        $result = $this->db->get('user');
        if($result->num_rows()==1){
            return $result->row(0);
        }else{
            return false;
        }
    }

    public function findUser() {
        $data = array(
            "email" => $this->input->post('email'),
        );

        $this->db->where($data);
        $result = $this->db->get('akun');
        return $result->result_array();
    }
}
?>