<!DOCTYPE html>
<html>

<head>
	<title> Home | Flickr </title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

	<link rel="icon" href="<?php echo base_url(); ?>img/ikon atas.png" />

	<script type="text/javascript">
		function confirmStatus() {
			if ($("#textStatus").val() != "") {
				if (confirm("Are you sure you want to cancel?")) {
					$("#textStatus").val("");
					setTimeout(function() {
						$("#modalStatus").modal("hide");
					}, 100);
				}
			} else if ($("#textStatus").val() == "") {
				$("#modalStatus").modal("hide");
			}
		}
		//menambahkan momen ke layar
		function postStatus() {
			if ($("#textStatus").val() != "" && $("#textStatus").val() != " ") {
				$("main").prepend("<section class=" + '"full-screen blue"' + " style=" + '"background-color: #0275d8; opacity:0.93; border-radius: 5px;"' + "> <p>" + $("#textStatus").val() + "</p>  </section> <button class=" + '"btn btn-danger"' + " style=" + '"bottom : 0; right : 0; color:white;opacity:0.93;"' + "> Delete </button>");
				$("#textStatus").val("");
				setTimeout(function() {
					$("#modalStatus").modal("hide");
				}, 100);
				$("section").removeAttr("#kosong");
			}
		}
		//menampilkan preview ke modal add picture
		function showImage(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function(e) {
					$('#gbr').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		}

		$(document).ready(function() {

			var ul = $("#navs"),
				li = $("#navs li"),
				i = li.length,
				n = i - 1,
				r = 120;
			ul.click(function() {
				$(this).toggleClass('active');
				if ($(this).hasClass('active')) {
					for (var a = 0; a < i; a++) {
						li.eq(a).css({
							'transition-delay': "" + (50 * a) + "ms",
							'-webkit-transition-delay': "" + (50 * a) + "ms",
							'left': (-r * Math.cos(90 / n * a * (Math.PI / 180))),
							'top': (-r * Math.sin(90 / n * a * (Math.PI / 180)))
						});
					}
				} else {
					li.removeAttr('style');
				}
			});

			$("#btnCari").click(function() {
				window.open("search_floment.html", "_self");
			});

		});
	</script>

	<style type="text/css">
		#logoFloment {
			border-radius: 10px;
			margin-top: 10px;
			margin-bottom: 10px;
		}

		#napigasi ul li {
			margin-left: 75px;
			font-family: patto;
			/* Font yang di download di dafont.com */
		}

		form {
			margin-right: 100px;
		}

		.footer {
			position: fixed;
			left: 0;
			bottom: 0;
			width: 100%;
			background-color: #D2F4FF;
			text-align: center;
			color: black;
			opacity: 0.93;
			font-family: patto;
			/* Font yang di download di dafont.com */
		}

		section {
			display: flex;
			justify-content: center;
			align-items: center;
			text-align: center;
			color: #EFEFEF;
			margin-left: 50px;
		}

		.full-screen {
			display: flex;
		}

		.blue {
			background-color: #3883d8;
		}

		.red {
			background-color: #cf3535;
		}

		.orange {
			background-color: #ea6300;
		}

		/*jquery plugin CSS start*/
		#navs {
			position: fixed;
			right: 30px;
			bottom: 4px;
			width: 50px;
			height: 50px;
			line-height: 45px;
			list-style-type: none;
			text-align: center;
			color: #fff;
			cursor: pointer;
			margin-bottom: 75px;
		}


		#navs>li,
		#navs:after {
			position: absolute;
			left: 0;
			top: 0;
			width: 100%;
			height: 100%;
			border-radius: 50%;
			-webkit-border-radius: 50%;
			background-color: #C70039;
			border-radius: 50px;
			-webkit-box-shadow: 1px 1px 3px 0px rgba(0, 0, 0, 1);
			-moz-box-shadow: 1px 1px 3px 0px rgba(0, 0, 0, 1);
			box-shadow: 1px 1px 3px 0px rgba(0, 0, 0, 1);
		}

		#navs>li {
			transition: all .6s;
			-webkit-transition: all .6s;
			-moz-transition: .6s;
		}

		#navs:after {
			content: attr(data-close);
			z-index: 1;
			border-radius: 50%;
			-webkit-border-radius: 50%;
		}

		#navs.active:after {
			content: attr(data-open);
		}

		/*jquery plugin CSS end*/

		/*CSS dari google MAPS start*/
		#map {
			width: 100%;
			height: 400px;
			background-color: grey;
		}

		.controls {
			margin-top: 10px;
			border: 1px solid transparent;
			border-radius: 2px 0 0 2px;
			box-sizing: border-box;
			-moz-box-sizing: border-box;
			height: 32px;
			outline: none;
			box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		}

		#pac-input {
			background-color: #fff;
			font-family: Roboto;
			font-size: 15px;
			font-weight: 300;
			margin-left: 12px;
			padding: 0 11px 0 13px;
			text-overflow: ellipsis;
			width: 300px;
		}

		#pac-input:focus {
			border-color: #4d90fe;
		}

		.pac-container {
			font-family: Roboto;
		}

		#type-selector {
			color: #fff;
			background-color: #4d90fe;
			padding: 5px 11px 0px 11px;
		}

		#type-selector label {
			font-family: Roboto;
			font-size: 13px;
			font-weight: 300;
		}

		#target {
			width: 345px;
		}

		/* CSS dari google maps end*/
		.modal {
			z-index: 520;
		}

		.modal-backdrop {
			z-index: 510;
		}

		@media (min-width: 768px) {

			/* ketika maximize*/
			main {
				margin-top: 250px;
			}

			section {
				min-height: 350px;
				max-height: 350px;
				min-width: 400px;
				max-width: 400px;
			}

			#logoFloment {
				margin-left: 50px;
				width: 150px;
			}

			#napigasi ul li {
				font-size: 20px;
			}

			#kolomSearch input {
				min-width: 300px;
				margin-right: 20px;
			}
		}

		@media (max-width: 768px) {

			/* versi mobile*/
			main {
				margin-top: 375px;
			}

			section {
				min-height: 200px;
				max-height: 200px;
				min-width: 250px;
				max-width: 250px;
			}

			#logoFloment {
				margin-left: 100px;
				width: 150px;
			}

			#napigasi ul li {
				margin-right: 100px;
				text-align: center;
			}

			#kolomSearch {
				justify-content: center;
				margin-left: 100px;
			}

			#kolomSearch button {
				margin-top: 10px;
			}
		}
	</style>

</head>

<body style="background-image: url('<?php echo base_url(); ?>img/BG.png'); background-size: 90%;">
	<!-- header navigation bar-->
	<div class="navbar navbar-default">
		<div class="container">
			<a href="#" class="navbar-brand">Flickr.com</a>
		</div>
	</div>
	<!-- ending header navigation bar-->
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #D2F4FF;top: 0;width:100%; position: fixed; opacity: 0.93; z-index: 500">
		<!--<a class="navbar-brand" href="<?php //echo site_url('Home_controller/index') ?>"><img src="<?php //echo base_url(); ?>img/logo1.jpg" id="logoFloment"></a>-->
		<div class="navbar-collapse" id="napigasi">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="<?php echo site_url('Home_controller/index') ?>"> You </a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo site_url('Home_controller/pindahProfile') ?>">Explore</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="" data-toggle="dropdown"> Create </a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="<?php echo site_url('Home_controller/pindahHelp') ?>">Help</a>
						<a class="dropdown-item" href="<?php echo site_url('Home_controller/pindahAbout') ?>">About</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="<?php echo site_url('Login_controller/logout') ?>">Log Out</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="" data-toggle="dropdown"> Get Pro</a>
					<div class="dropdown-menu" style="width: 300px; height: 400px;">

					</div>
				</li>
			</ul>
			<form class="form-inline" id="kolomSearch">
				<input class="form-control" type="search" placeholder="Search People">
				<a class="btn btn-outline-primary" id="btnCari">Search</a>
			</form>
		</div>
	</nav>
	<div>
		<main class="full-screen">

		</main>
	</div>
	<!-- JQUERY PLUGIN-->
	<ul id="navs" data-open="&times;" data-close="+" style="font-size: 30px;">
		<li data-toggle="modal" data-target="#modalLokasi"><img src="<?php echo base_url(); ?>img/placeholder.png" style="width: 20px"></li>
		<li data-toggle="modal" data-target="#modalGambar"><img src="<?php echo base_url(); ?>img/photo-camera.png" style="width: 30px"></li>
		<li data-toggle="modal" data-target="#modalStatus"><img src="<?php echo base_url(); ?>img/pencil-edit-button.png" style="width: 20px;"></li>
	</ul>

	<div class="footer"> <br>
		<p> Flickr &copy; 2018</p>
	</div>

	<!-- modal untuk add moment -->
	<div class="modal fade" id="modalStatus">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Write A Moment Here...</h5>
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<textarea class="form-control" rows="5" id="textStatus"></textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" onclick="confirmStatus()">Cancel</button>
					<button type="button" class="btn btn-primary" id="btnStatus" onclick="postStatus()">Post</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalGambar">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Post Image</h5>
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<input type="file" accept="image/*" onchange="showImage(this)"> <img id="gbr" src="" style="width: 100%">
					</form> <br>
					<p>Add a caption...</p>
					<textarea class="form-control" rows="5" id=""></textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal" ">Cancel</button>
	        <button type=" button" class="btn btn-primary" data-dismiss="modal">Post</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalLokasi" style="">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Share Your Location</h5>
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<input id="pac-input" class="controls" type="text" placeholder="Search Location">
					<div id="map"></div>
					<br>
					<p>Add a description...</p>
					<textarea class="form-control" rows="5" id=""></textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal">Post</button>
				</div>
			</div>
		</div>
	</div>

	<script>
		/* Codingan Murni dari GOOGLE MAPS untuk menambahkan maps ke website*/
		function initAutocomplete() {
			var map = new google.maps.Map(document.getElementById('map'), {
				center: {
					lat: -33.8688,
					lng: 151.2195
				},
				zoom: 13,
				mapTypeId: 'roadmap'
			});


			var input = document.getElementById('pac-input');
			var searchBox = new google.maps.places.SearchBox(input);
			map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);



			map.addListener('bounds_changed', function() {
				searchBox.setBounds(map.getBounds());
			});

			var markers = [];

			searchBox.addListener('places_changed', function() {
				var places = searchBox.getPlaces();

				if (places.length == 0) {
					return;
				}

				markers.forEach(function(marker) {
					marker.setMap(null);
				});
				markers = [];


				var bounds = new google.maps.LatLngBounds();
				places.forEach(function(place) {
					if (!place.geometry) {
						console.log("Returned place contains no geometry");
						return;
					}
					var icon = {
						url: place.icon,
						size: new google.maps.Size(71, 71),
						origin: new google.maps.Point(0, 0),
						anchor: new google.maps.Point(17, 34),
						scaledSize: new google.maps.Size(25, 25)
					};

					markers.push(new google.maps.Marker({
						map: map,
						icon: icon,
						title: place.name,
						position: place.geometry.location
					}));

					if (place.geometry.viewport) {
						bounds.union(place.geometry.viewport);
					} else {
						bounds.extend(place.geometry.location);
					}
				});
				map.fitBounds(bounds);
			});
		}
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALTHBFLLHCtqpWyqAECx5c-FQkUYo0eQg&libraries=places&callback=initAutocomplete" async defer></script>
</body>

</html>