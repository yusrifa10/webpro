<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login_Controller extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('database_flickr');
	}

	public function index()
	{
		$this->load->view('Login_View');
	}
	public function PindahLogin()
	{
		$this->load->view('Login');
	}

	public function pindahProfile()
	{
		redirect('Profile_controller/index');
	}

	public function pindahSignup()
	{
		redirect('Signup_controller/index');
	}
    public function Signin() {
        $user = $this->database_flickr->findUser();
        if($user != null){
            $this->session->set_userdata('successLogin', $user[0]['Username']);
            redirect('Home_Controller');
        }
		else {
            $this->session->set_flashdata('falselogin','nodata');
            redirect('Login_Controller/pindahLogin');
        }
    }
	public function ceklogin()
	{
		$data = $this->input->post(null, TRUE);
		$login = $this->database_flickr->login_akun($data);
		if ($login) {
			$datasession = array(
				'namadepan' => $login->nama_depan,
				'namabelakang' => $login->nama_belakang,
				'status' => "sudah login"
			);

			$this->session->set_userdata($datasession);

			redirect('Home_Controller/index');
		} else {
			//echo "USERNAME ATO PASSWORD SALAH";
			redirect('Login_Controller/index');
		}
	}
	public function logout()
	{
		$this->session->sess_destroy();

		redirect('Login_Controller/index');
	}
}
