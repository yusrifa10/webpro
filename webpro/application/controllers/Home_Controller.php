<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home_controller extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
	}

	public function index()
	{
		//session_start();

		//if($this->session->flashdata()){
		$this->load->view('Home_view');
		//} else {
		//redirect('Login_controller/index');
		//}
	}

	public function pindahProfile()
	{
		redirect('Profile_controller/index');
	}
	public function pindahHelp()
	{
		redirect('Help_controller/index');
	}
	public function pindahAbout()
	{
		redirect('About_controller/index');
	}
	public function pindahHalamanLogin()
	{
		redirect('Login_Controller/pindahLogin');
	}
}
