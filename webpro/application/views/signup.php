<!DOCTYPE html>
<html>

<head>
    <title>Flickr</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <link href="<?php echo base_url('assets/js/datepicker/daterangepicker.css'); ?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/js/select2/select2.min.css'); ?>" rel="stylesheet" media="all">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <link rel="icon" href="img/ikon atas.png" />

    <script type="text/javascript">
        var i = 0;

        $(document).ready(function() {
            $("#btnLogin").click(function() {
                <?php
                //session_start();

                //$myusername=$_POST['emailuser'];
                //form_open("Login_controller/ceklogin");
                //form_close();
                ?>
            });
        });
    </script>

    <style type="text/css">
        .navbar {
            padding: 0;
            padding-bottom: 5px;
            background-color: #212124;
        }

        .container-fluid {
            background-image: url('<?php echo base_url('img/13.jpg') ?>');
            background-position: 30% 60%;
            background-size: cover;
            height: 100vh;
            position: fixed;
        }

        .logo-brand {
            height: 20px;
        }

        .navbar-brand {
            align-items: center;
        }

        .menu-middle {
            padding: 24px;
            background-color: white;
            opacity: 0.9;
            width: auto;
            margin: 0;
            position: absolute;
            top: 50%;
            left: 50%;
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            border-radius: 5px;
            -webkit-box-shadow: -5px 2px 12px -9px rgba(0, 0, 0, 0.41);
            -moz-box-shadow: -5px 2px 12px -9px rgba(0, 0, 0, 0.41);
            box-shadow: -5px 2px 12px -9px rgba(0, 0, 0, 0.41);
        }

        .logo-biru-merah {
            height: 30px;
        }

        .menu-item {
            margin-bottom: 10px;
        }

        .menu-title {
            font-size: 32px;
        }

        .menu-description {
            text-align: center;
            font-size: 16px;
        }

        .signup-image {
            position: fixed;
        }

        .signup-form {
            font-size: 20px;
            margin: 2px;
            padding: 5px;
        }

        .btn-submit {
            width: 100%;
        }

        /* ==========================================================================
   #DATE PICKER
   ========================================================================== */
        td.active {
            background-color: #2c6ed5;
        }

        input[type="date"i] {
            padding: 14px;
        }

        .table-condensed td,
        .table-condensed th {
            font-size: 14px;
            font-family: "Roboto", "Arial", "Helvetica Neue", sans-serif;
            font-weight: 400;
        }

        .daterangepicker td {
            width: 40px;
            height: 30px;
        }

        .daterangepicker {
            border: none;
            -webkit-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            -moz-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            display: none;
            border: 1px solid #e0e0e0;
            margin-top: 5px;
        }

        .daterangepicker::after,
        .daterangepicker::before {
            display: none;
        }

        .daterangepicker thead tr th {
            padding: 10px 0;
        }

        .daterangepicker .table-condensed th select {
            border: 1px solid #ccc;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            font-size: 14px;
            padding: 5px;
            outline: none;
        }

        /* ==========================================================================
   #SELECT2
   ========================================================================== */
        .select--no-search .select2-search {
            display: none !important;
        }

        .rs-select2 .select2-container {
            width: 100% !important;
            outline: none;
        }

        .rs-select2 .select2-container .select2-selection--single {
            outline: none;
            border: none;
            height: 36px;
            background: transparent;
        }

        .rs-select2 .select2-container .select2-selection--single .select2-selection__rendered {
            line-height: 36px;
            padding-left: 0;
            color: #ccc;
            font-size: 16px;
            font-family: inherit;
        }

        .rs-select2 .select2-container .select2-selection--single .select2-selection__arrow {
            height: 34px;
            right: 4px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -moz-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -moz-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }

        .rs-select2 .select2-container .select2-selection--single .select2-selection__arrow b {
            display: none;
        }

        .rs-select2 .select2-container .select2-selection--single .select2-selection__arrow:after {
            font-family: "Material-Design-Iconic-Font";
            content: '\f2f9';
            font-size: 18px;
            color: #ccc;
            -webkit-transition: all 0.4s ease;
            -o-transition: all 0.4s ease;
            -moz-transition: all 0.4s ease;
            transition: all 0.4s ease;
        }

        .rs-select2 .select2-container.select2-container--open .select2-selection--single .select2-selection__arrow::after {
            -webkit-transform: rotate(-180deg);
            -moz-transform: rotate(-180deg);
            -ms-transform: rotate(-180deg);
            -o-transform: rotate(-180deg);
            transform: rotate(-180deg);
        }

        .select2-container--open .select2-dropdown--below {
            border: none;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            -webkit-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            -moz-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            border: 1px solid #e0e0e0;
            margin-top: 5px;
            overflow: hidden;
        }
    </style>
</head>

<body>
    <!-- starting navbar-->
    <nav class="navbar navbar-expand-md">
        <div class="div-brand mx-auto w-75">
            <a class="navbar-brand" href="#"><img class="logo-brand" src="<?php echo base_url('img/logo.png') ?>" alt="Logo"></a>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="menu-middle">
            <div class="inner-menu">
                <div class="menu-item biru-merah d-flex justify-content-center">
                    <img class="logo-biru-merah" src="<?php echo base_url('img/ikon atas.png') ?>" alt="ikon">
                </div>
                <div class="menu-item menu-title d-flex justify-content-center">
                    SignUp to Flickr
                </div>
                <div class="container">
                    <div class="signup-content">
                        <div class="signup-form">
                            <form action="<?php echo site_url('Signup_controller/signup'); ?>" method="POST" class="register-form" id="register-form">
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col">
                                            <input type="text" class="form-control" placeholder="First name">
                                        </div>
                                        <div class="col">
                                            <input type="text" class="form-control" placeholder="Last name">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Username" required />
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col-md-8">
                                            <input class="form-control js-datepicker" type="text" placeholder="Birthdate" name="birthday">
                                        </div>
                                        <div class="col-md-4">

                                            <select class="custom-select" name="gender" style="font-family:Arial; font-size:16px;>
                                                <option disabled=" disabled" selected="selected">Gender</option>
                                                <option value="L">Male</option>
                                                <option value="P">Female</option>
                                                <option>Other</option>
                                            </select>
                                            <div class="select-dropdown"></div>

                                        </div>
                                    </div>
                                    <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required />
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="pass" id="pass" placeholder="Password" required />
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="re_pass" id="re_pass" placeholder="Repeat your password" required />
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" required />
                                    <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all statements in <a href="#" class="term-service">Terms of service</a></label>
                                </div>
                                <div class="form-group form-button">
                                    <input type="submit" class="btn btn-primary btn-submit" name="signup" id="signup" class="form-submit" value="Register" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="signup-image">
            <figure><img src="<?php echo base_url('img/signup-image2.png'); ?>" alt="sing up image"></figure>
            <a href="<?php echo site_url('Login'); ?>" class="signup-image-link">I am already member</a>
        </div>
    </div>
    <!-- Jquery JS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="<?php echo base_url('assets/js/select2/select2.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/datepicker/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/datepicker/daterangepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/global.js'); ?>"></script>
</body>

</html>