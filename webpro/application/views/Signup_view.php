<!DOCTYPE html>
<html>
<head>
	<title>Sign Up</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"> </script>

	<link rel="icon" href="img/ikon atas.png" />

	<style type="text/css">
		p {
			margin-top: 75px;
		}
		form {
			margin-top: 50px;
		}
		body {
			background-image: url("<?php echo base_url(); ?>img/BG-Signup.jpg");

		}
		a {
			margin-bottom: 50px;
		}
		@media (min-width: 768px) {
			body{
				background-size: 100%;
			}
		}
		@media (max-width: 768px) {
			body{
				background-size: 250%;
			}
			#konten {
				font-size: 13px;
			}
			label {
				margin-right: 150px;
			}
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-12 text-center h1">
				<p style="font-family: patto"> Sign Up, It's Free! </p>
			</div>
			<div class="col-7" style="margin: 0 auto;" id="konten">
			<!--form class="form-horizontal"-->
			<!--<?php // echo form_open('Signup_controller/daftar_akun'); ?>-->
			 	<div class="form-group row">
				 	<label class="col-6 col-form-label"> First Name </label>
				 	<input type="text" class="col-12 form-control" placeholder="First Name" name="namadepan">

			 		<label class="col-6 col-form-label" style="margin-top: 20px;"> Last Name </label>
			 		<input type="text" class="col-12 form-control" placeholder="Last Name" name="namabelakang">
			 	</div>

			 	<div class="form-group row">
				 	<label class="col-2 col-form-label"> Gender </label>
   					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" name="gender" value="Laki-laki"> <div class="col-1" > Male </div>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" name="gender" class="col-form-label" value="Perempuan"> <div class="col-1" > Female </div>
					</div>
			 	</div>

			 	<div class="form-group row">
				 	<label class="col-6 col-form-label"> Date of Birth </label>
   					<input class="col-12 form-control" type="date" value="YYYY-MM-DD" name="tglLahir">
			 	</div>

			 	<div class="form-group row">
			 		<label class="col-6 col-form-label"> E-mail </label>
			 		<input type="Email" class="col-12 form-control" placeholder="E-mail Address" name="email">
			 	</div>

			 	<div class="form-group row">
					<label class="col-6 col-form-label"> Password </label>
				 	<input type="password" class="col-12 form-control" placeholder="Password" name="password">
			 	</div>
			 	<br>
			 	<div class="row">
			 		<button class="btn btn-primary btn-block">Sign Up</button>
			 	</div>
			</form>
		</div>
		</div>
	</div>
</body>
</html>