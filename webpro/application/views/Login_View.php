<!DOCTYPE html>
<html>

<head>
	<title>Flickr</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

	<link rel="icon" href="img/ikon atas.png" />

	<script type="text/javascript">
		var i = 0;

		$(document).ready(function() {
			$("#btnLogin").click(function() {
				<?php
				//session_start();

				//$myusername=$_POST['emailuser'];
				//form_open("Login_controller/ceklogin");
				//form_close();
				?>
			});
		});
	</script>

	<style type="text/css">
		.container {
			padding-top: 100px;
		}

		.footer {
			position: absolute;
			left: 0;
			bottom: 0;
			width: 100%;
			height: 8%;
			background-color: #343a40;
			text-align: center;
			color: white;
		}

		.logo-brand {
			height: 20px;
		}

		.navbar-brand {
			align-items: center;
		}

		.btn {
			font-size: 25px;
			color: black;
			background-color: white;
			height: 70px;
			width: 50px;
		}

		#shadowing {
			color: white;
			text-shadow: -1px -1px 0 #000000,
				1px -1px 0 #000000,
				-1px 1px 0 #000000,
				1px 1px 0 #000000;
		}

		@media (min-width: 768px) {
			form {
				margin-top: 300px;
			}

			#gambar {
				width: 1540px;
				height: 850px;
			}
		}

		@media (max-width: 768px) {
			form {
				margin-top: 200px;
			}

			#gambar {
				height: 800px;
				width: 500px;
			}
		}
	</style>

	<script type="text/javascript">
		var BackgroundImg = [
			"img/3a.jpg",
			"img/1.jpg",
			"img/2.jpg",
			"img/3.jpg",
			"img/4.jpg",
			"img/9.jpg",

		];
		$(function() {
			var i = 0;
			$("#gambar").css("background-image", "url(" + BackgroundImg[i] + ")");
			setInterval(function() {
				i++;
				if (i == BackgroundImg.length) {
					i = 0;
				}
				$("#gambar").fadeOut("slow", function() {
					$(this).css("background-image", "url(" + BackgroundImg[i] + ")");
					$(this).fadeIn("slow");
				});
			}, 3000);
		});
	</script>
</head>

<body>
	<div id="gambar" style="position: fixed;"></div>
	<!-- starting navbar-->
	<nav class="navbar navbar-expand-md navbar-dark bg-dark " style="opacity: 0.8;">
		<a class="navbar-brand" href="#"><img class="logo-brand" src="<?php echo base_url('img/logo.png') ?>" alt="Logo"></a>
		<button class="navbar-toggler" type="button" data-toggler="collapse" data-target="#navbarSupportedContent">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<form class="form-line" style="margin-top: 0%; margin-left: 10%;">
				<input class="form-control mr-sm-2" type="search" placeholder="Photos, people or groups" aria-label="Search" style="width: 450%">
			</form>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active">
					<a class="nav-link" href="<?php echo site_url('Home_Controller/pindahHalamanLogin') ?>">Log in</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="index.php/auth_sample/sign_up">Sign Up</a>
				</li>
			</ul>
		</div>
	</nav>

	<div class="container">
		<div class="row">
			<div class="col-12 text-center" style="margin-top: 6.5%">
				<nobr style="font-family: Lemon/Milk;font-size: 50px; color:white;" id="logoName"> <b>Find your inspiration.</b></nobr>
			</div>
			<div class="col-12 text-center" style="margin-top: 6.5%">
				<nobr style="font-family: Lemon/Milk;font-size: 30px; color:white;" id="logoName"> Join the Flickr community, home to tens of billions of
					photos and 2 million groups. </nobr>
			</div>
			<div class="col-8" style="margin: 0 auto">

				<?php
				$atribut = array(
					'class' => 'form-horizontal'
				);
				?>

				<?php
				?>
				<!--	<div class="form-group">
						<input type="text" class="form-control" name="email" placeholder="E-mail" style="text-align: center" id="emailuser">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" name="password" placeholder="Password" style="text-align: center" id="passworduser">
					</div> -->
				<center>
					<input type="submit" class="btn btn-primary btn-block" id="btnLogin" value="Sign Up" style="width: 20%; margin-top: 4%"> <br>
				</center>


				</form>
			</div>
		</div>

		<div class="footer"> <br>
			<p> Flickr &copy; 2018</p>
		</div>
	</div>
</body>

</html>